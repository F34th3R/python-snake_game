#
# f34th3r.io
#

import turtle
import time
import random

# from lib.Pila import Pila


class Pila(object):

    def __init__(self):
        self.items=[]

    def apilar(self, dato):
        self.items.append(dato)

    def mostrar(self, index):
        if self.esta_vacia():
            return None
        else:
            return self.items[index]

    def desapilar(self):
        if self.esta_vacia():
            return None
        else:
            return self.items.pop()

    def esta_vacia(self):
        if len(self.items) == 0:
            return True
        else:
            return False



banner_bottom = Pila()

windows_title = "Snake Game by f34th3r"
banner_bottom.apilar("f34th3r.io // info@f34th3r.io")
windows_size_w = 600
windows_size_h = 600
snake_head_color = "white"
snake_body_color = "white"
background_color = "black"
food_color = "purple"
score_color = "white"


#########################
delay = 0.1
score = 0
high_score = 0
scr_bottom = banner_bottom.mostrar(0)

# screen
window = turtle.Screen()
window.title(windows_title)
window.bgcolor(background_color)
window.setup(width=windows_size_w, height=windows_size_h)
# turns off the screen updates
window.tracer(0)


# Snake Head
head = turtle.Turtle()
head.speed(0)
head.shape("square")
head.color(snake_head_color)
head.penup()
head.goto(0, 0)
head.direction = "stop"

# Snake food
food = turtle.Turtle()
food.speed(0)
food.shape("circle")
food.color(food_color)
food.penup()
food.goto(0, 100)
food.direction = "stop"


# List
segments = []
# Pen
pen = turtle.Turtle()
pen.speed(0)
pen.shape("square")
pen.color(score_color)
pen.penup()
pen.hideturtle()
pen.goto(0, (windows_size_h / 2) - 40)
pen.write("Score: 0   High Score: 0", align="center", font=("Courier", 20, "normal"))

pen2 = turtle.Turtle()
pen2.speed(0)
pen2.shape("square")
pen2.color(score_color)
pen2.penup()
pen2.hideturtle()
pen2.goto(0, -((windows_size_h / 2) - 10))
pen2.write(scr_bottom, align="center", font=("Courier", 10, "normal"))


# Functions
def go_up():
    if head.direction != "down":
        head.direction = "up"


def go_down():
    if head.direction != "up":
        head.direction = "down"


def go_left():
    if head.direction != "right":
        head.direction = "left"


def go_right():
    if head.direction != "left":
        head.direction = "right"


def stop_snake():
    head.direction = "stop"


def move():
    if head.direction == "up":
        head.sety(head.ycor() + 20)

    if head.direction == "down":
        head.sety(head.ycor() - 20)

    if head.direction == "left":
        head.setx(head.xcor() - 20)

    if head.direction == "right":
        head.setx(head.xcor() + 20)


def print_score(_score, _high_score):
    pen.clear()
    pen.write("Score: {}   High Score: {}".format(_score, _high_score), align="center",
              font=("Courier", 20, "normal"))


def game_over():
    time.sleep(1)
    head.goto(0, 0)
    head.direction = "stop"
    # Hide the segments
    for segment in segments:
        segment.goto(windows_size_w + 600, windows_size_h + 600)

    # clear the segments list
    segments.clear()


# keyboard bindings
window.listen()
window.onkeypress(go_up, "w")
window.onkeypress(go_down, "s")
window.onkeypress(go_left, "a")
window.onkeypress(go_right, "d")
window.onkeypress(stop_snake, "x")

# Main game loop
while True:
    window.update()

    # Check for a collision with the border
    if head.xcor() > (windows_size_w/2) - 10 or head.xcor() < -((windows_size_w/2) - 10) \
            or head.ycor() > (windows_size_h/2) - 10 or head.ycor() < -((windows_size_h/2) - 10):
        game_over()
        # reset the score
        delay = 0.1
        score = 0
        print_score(score, high_score)

    # check for a collision ## Food
    if head.distance(food) < 20:
        # Move the food to random place
        food.goto(random.randint(-((windows_size_h/2) - 10), (windows_size_h/2) - 10),
                  random.randint(-((windows_size_h/2) - 10), (windows_size_h/2) - 10))

        # Add a segment
        new_segment = turtle.Turtle()
        new_segment.speed(0)
        new_segment.shape("square")
        new_segment.color(snake_body_color)
        new_segment.penup()
        segments.append(new_segment)

        # Shorten delay
        delay -= 0.001

        # Increase the score
        score += 10

        if score > high_score:
            high_score = score

        print_score(score, high_score)

    # Move the end segments first in reverse order
    for index in range(len(segments) -1, 0, -1):
        x = segments[index - 1].xcor()
        y = segments[index - 1].ycor()
        segments[index].goto(x, y)

    # Move segment 0 to where the head is
    if len(segments) > 0:
        segments[0].goto(head.xcor(), head.ycor())

    move()
    # check for head collision with the body
    for segment in segments:
        if segment.distance(head) < 20:
            game_over()
            delay = 0.1
            score = 0
            print_score(score, high_score)

    time.sleep(delay)


window.mainloop()